// function input(){
//     let nickName = prompt("What is your nickname?");
//     console.log("Your nickname is: "+nickName);
// }
// input();

function printName(name){
    console.log("hello " + name);

}
printName("Joana");

// PARAMETER it acts as a named variable /container that exists only inside of a function;
// parameter is inside the function ex. function printName(name){}

printName("John");
printName("Jane");

let sampleVar = "Yua";
printName(sampleVar);

function passedNumber(firstNumber,secondNumber){
    console.log("The numbers passed as arguments are: ");
    console.log(firstNumber);
    console.log(secondNumber);
}
passedNumber(3,5);
function myFriends(firstFriend, secondFriend, thirdFriend){
    console.log("My friends are "+firstFriend+", " +secondFriend+", "+thirdFriend);
}
myFriends("Joseph","Carrin","Jacob");

function checkDivisibilityBy8(num){
    let remainder = num%8;
    console.log("The remainder of "+num+ " is "+remainder+".");
    let isDivisibleBy8 = remainder === 0;
    console.log("Is "+ num+" divisible by 8?");
    console.log(isDivisibleBy8);
}
checkDivisibilityBy8(64);

function checkDivisibilityBy4(num){
    let remainder = num%4;
    console.log("The remainder of "+num+ " is "+remainder+".");
    let isDivisibleBy4 = remainder === 0;
    console.log("Is "+ num+" divisible by 4?");
    console.log(isDivisibleBy4);
}
checkDivisibilityBy4(1250);

function isEven(num){
	console.log(num%2 === 0);
}

function isOdd(num1){
	console.log(num1%2 !== 0);

}

let numEven = isEven(20);
let numOdd = isOdd(31);

// console.log(numEven);
// console.log(numOdd);

function argumentFunction(){
    console.log("This function is passed into another function.")
}
function invokeFunction(functionParameter){
    functionParameter();
}

invokeFunction(argumentFunction);

function printFullName(firstName,middleName,lastName){
    console.log(firstName+" "+middleName+" "+lastName);
}
printFullName("Juan","Dela","Cruz");

// return statement
function returnFullName(firstName,middleName,lastName){
    return firstName + " " +middleName+" "+lastName;
    // console.log("This message will not be printed");
}
console.log(returnFullName("Jeffrey","Smith","Bezros"));

let completeName = returnFullName("Jeffrey","Smith","Bezros");
console.log(completeName);

function returnAddress(cityAddress,countryAddress){
    return cityAddress +", "+countryAddress;
}
console.log(returnAddress("Scarborough","Canada"));

function printPlayerInfo(username,level,job){
    console.log("Username: "+username);
    console.log("Level: "+level);
    console.log("Job: "+job);

}
let user1  = printPlayerInfo("Knight_white",95,"Paladin");
console.log(user1);